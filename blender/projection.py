import numpy as np
import matplotlib.pyplot as plt

# everything is in meters / radians


# lens (positioned in the center)

r_lens = 0.1
x_lens = 0.0
y_lens = 0.0


# projection screen

r_screen = 0.4
x_screen = 0.3
y_screen = 0.44


# location of the center of the projection screen in polar
psi = np.arctan(y_screen / x_screen)
r0 = x_screen / np.cos(psi)


# lens equation in polar
R_lens = lambda phi: 0.1  # const because it's in the center

# screen equation in polar (+ in the middle cause we consider the farther part of the screen)
R_screen = lambda phi: r0 * np.cos(phi - psi) + np.sqrt(r_screen**2 - (r0**2) * (np.sin(phi - psi)**2))


# distance between lens / screen
D = lambda phi: R_screen(phi) - R_lens(phi)


x = np.arange(10, 75, 1.0);
y = np.array([D(np.pi * (ang/180.0)) for ang in x])
plt.plot(x, y)
plt.show()
