import serial
import time
from os import sys, path

if __name__ == '__main__':
    """
    Connects to a (connected) Arduino device and sends 1 to it every 4 sec.
    """

    abs_path = path.abspath(__file__)
    pkg_path = reduce(lambda x, y: path.dirname(x) + y, ['', '', ''], abs_path)

    sys.path.append(pkg_path)
    from arduino.ports import serial_ports

    try:
        port = [x for x in serial_ports() if 'ACM' in x][0]
    except IndexError:
        raise IOError("Cannot find Arduino.")

    baud = 115200

    arduino = serial.Serial(port, baud, timeout=.1)
    time.sleep(1)

    while True:
        arduino.write("1")
        time.sleep(4)
