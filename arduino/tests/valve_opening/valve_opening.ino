
/*
  Sketch to control the valve from PC (via serial input) or using a switch.

  When the input (1) is detected via serial the valve is opened for a short 
  period of time.
 */

// Arduino pins for the shift register
const int
motorLatch  = 12,
motorClk    = 4,
motorEnable = 7,
motorData   = 8;

// 8-bit bus after the 74HC595 shift register 
// (not Arduino pins)
// These are used to set the direction of the bridge driver.
const int
motor1A = 2,
motor1B = 3,
motor2A = 1,
motor2B = 4,
motor3A = 5,
motor3B = 7,
motor4A = 0,
motor4B = 6;

// Arduino pins for the PWM signals.
const int
motor1PWM = 11,
motor2PWM = 3,
motor3PWM = 6,
motor4PWM = 5,
servo1PWM = 10,
servo2PWM = 9;

// Codes for the motor function.
const int
motorForward  = 1,
motorBackward = 2,
motorBrake    = 3,
motorRelease  = 4;

// pins for the valve and the switch
const int
valvePin    = motor3A,
switchPin   = 1;

char data = '0';      // input data from PC
int switchState = 0;  // valve switch state
int latchCopy = 0;    // start with all outputs (of the shift register) low


void setup() {
  // valve switch
  pinMode(switchPin, INPUT);

  // serial communication
  Serial.begin(115200);
  
  // Motor Shield setup
  pinMode(motorLatch, OUTPUT);
  pinMode(motorEnable, OUTPUT);
  pinMode(motorData, OUTPUT);
  pinMode(motorClk, OUTPUT);

  digitalWrite(motorData, LOW);
  digitalWrite(motorLatch, LOW);
  digitalWrite(motorClk, LOW);
  digitalWrite(motorEnable, LOW);
}

void loop() {
  // check if switch is ON
  switchState = analogRead(switchPin);

  // read input from PC
  if (Serial.available() > 0) {
    data = Serial.read();
  }

  // valve ON / OFF logic
  if (switchState > 500) {
    shiftWrite(valvePin, HIGH); // open valve
  }
  else if (data == '1') {
    shiftWrite(valvePin, HIGH); // open valve
    delay(1000);
    shiftWrite(valvePin, LOW);  // close valve
    delay(1000);

    data = 0;
  }
  else {
    shiftWrite(valvePin, LOW);  // close valve
  }
}

/*
  shiftWrite - for Motor Shield
  http://playground.arduino.cc/Main/AdafruitMotorShield
  
  The parameters are just like digitalWrite().

  The output is the pin 0...7 (the pin behind
  the shift register).
  The second parameter is HIGH or LOW.

  There is no initialization function.
  Initialization is automatically done at the first
  time it is used.
 */
void shiftWrite(int output, int high_low)
{
  // The defines HIGH and LOW are 1 and 0.
  // So this is valid.
  bitWrite(latchCopy, output, high_low);

  // Use the default Arduino 'shiftOut()' function to
  // shift the bits with the motorClk as clock pulse.
  // The 74HC595 shiftregister wants the MSB first.
  // After that, generate a latch pulse with motorLatch.
  shiftOut(motorData, motorClk, MSBFIRST, latchCopy);
  delayMicroseconds(5);    // For safety, not really needed.
  digitalWrite(motorLatch, HIGH);
  delayMicroseconds(5);    // For safety, not really needed.
  digitalWrite(motorLatch, LOW);
}
