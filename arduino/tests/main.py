import serial
import time
from os import sys, path

if __name__ == '__main__':
    """
    Connects to a (connected) Arduino device and prints its output.
    """

    abs_path = path.abspath(__file__)
    pkg_path = reduce(lambda x, y: path.dirname(x) + y, ['', '', ''], abs_path)

    sys.path.append(pkg_path)
    from arduino.ports import serial_ports

    try:
        if sys.platform.startswith('win'):
            port = [x for x in serial_ports()][-1]  # TODO upgrade serial and do via list(serial.tools.list_ports.comports())
        elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
            port = [x for x in serial_ports() if 'ACM' in x][0]
    except IndexError:
        raise IOError("Cannot find Arduino.")

    baud = 115200

    arduino = serial.Serial(port, baud, timeout=.1)

    while True:
        data = arduino.readline()[:-2]
        if data:
            if int(data) == 1:
                arduino.write("1")
                print("sending a pulse to open a valve..")
                
            print(arduino.readline()[:-2])
