/*
  Simple sketch to test the beam break.
 
  Prints to Serial and blinks LED
    1 - beam is OK (LED on)
    0 - beam is broken (LED off)
 */

int val = 0;
int threshold = 50;

// the setup function runs once when you press reset or power the board
void setup() {
  pinMode(13, OUTPUT);
  
  Serial.begin(115200);
}

// the loop function runs over and over again forever
void loop() {
  val = analogRead(0);

  Serial.println(val);
  
  if (val > threshold) {
    digitalWrite(13, HIGH); // LED on
  } else {
    digitalWrite(13, LOW);  // LED off
  }
  
  delay(50);
}
