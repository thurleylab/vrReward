/*

  Tutorials
  http://zelectro.cc/Adafruit_motor_shield
  http://www.14core.com/controlling-the-l293d-unomega-shield-with-stepper-motor/


  If your going to turn the 2 motor step one once you need to change the code same below

  void doublestep (int steps, int direction, int style) {
  while (steps--) {
    motor1.step(1, Motor direction, Turn Style);
    motor2.step(1, MOtor direction, Turn Style);
  }
  }

*/

#include <AFMotor.h>

#define INPUT_SIZE 30
#define MAX_MANUAL_SPINS 75000         // ~2000 is one turn
#define SPEED_MANUAL_CALIBRATION 100   // speed while calibrating in manual mode
#define SPEED_MANUAL_OPERATIONAL 800   // speed while operating in manual mode
#define SPEED_SERIAL_OPERATIONAL 500   // speed while operating during experiment via serial port

const int photoPin = 0;
const int switchFWD = 1;
const int switchREW = 2;

int m1Steps = 0;
int m1Direction = FORWARD;
int m2Steps = 0;
int m2Direction = FORWARD;

int photoIn = 0;      // input from photodiode
int stateFWD = 0;     // input manual motor FORWARD
int stateREW = 0;     // input manual motor BACKWARD
float dataIn = 0.0;   // input from computer

long safetyFlag = 0;   // flag to prevent motor to burn when on manual mode
int collector = 0;    // in case safetyFlag suddenly resets 

int mode = 0;         // 0 - operational (normal), 1 - calibration mode for port position adjustment

AF_Stepper motor1(48, 1);  // syringe
AF_Stepper motor2(48, 2);  // port rotation


void setup() {
  Serial.setTimeout(50);
  Serial.begin(115200);

  motor1.setSpeed(SPEED_SERIAL_OPERATIONAL);
  motor1.release();

  if (mode > 0) { // calibration mode
    motor2.setSpeed(SPEED_MANUAL_CALIBRATION);
  } else {
    motor2.setSpeed(SPEED_MANUAL_OPERATIONAL);
  }
  motor2.release();

  pinMode(switchFWD, INPUT);
  pinMode(switchREW, INPUT);
}

void loop() {
  stateFWD = analogRead(switchFWD);
  stateREW = analogRead(switchREW);
  photoIn = analogRead(photoPin);

  Serial.println(photoIn);
  
  if (stateFWD > 800 || stateREW > 800) {  // manual mode, first priority

    if (safetyFlag == 0) {
      motor1.setSpeed(SPEED_MANUAL_OPERATIONAL);
    }

    if (stateFWD > 800) {  // return syringe lever back

      if (safetyFlag < MAX_MANUAL_SPINS) {
        safetyFlag++;
        collector = 0;

        if (mode > 0) {
          motor2.step(1, FORWARD, DOUBLE);
        } else {
          motor1.step(1, FORWARD, DOUBLE);
        }
      }
    }
    
    else if (stateREW > 800) {  // pushing the syringe
      if (mode > 0) {
        motor2.step(1, BACKWARD, DOUBLE);
      } else {
        motor1.step(1, BACKWARD, DOUBLE);
      }
    };
  }
  
  else {  // PC-controlled mode

    if (safetyFlag > 0) {
      collector++;  // 
    }

    if (collector > 5) {
      safetyFlag = 0; // reset manual mode
      collector = 0;

      motor1.setSpeed(SPEED_SERIAL_OPERATIONAL);
    }
    
    //dataIn = Serial.parseFloat();

    // Get next command from Serial (add 1 for final 0)
    char input[INPUT_SIZE + 1];
    byte size = Serial.readBytes(input, INPUT_SIZE);
    
    // Add the final 0 to end the C string
    input[size] = 0;
    
    // Read each command pair 
    char* command = strtok(input, "&");
    
    while (command != 0)
    {
        // Split the command in two values
        char* separator = strchr(command, ':');
        
        if (separator != 0)
        {
            // Actually split the string in 2: replace ':' with 0
            *separator = 0;
            int servoId = atoi(command);
            
            ++separator;
            int pos = atoi(separator);
    
            if (servoId == 1) {
              if (pos > 0) {
                m1Steps += pos;
                m1Direction = FORWARD;
              } else {
                m1Steps += -pos;
                m1Direction = BACKWARD;               
              }
            }

            if (servoId == 2) {
              if (pos > 0) {
                m2Steps += pos;
                m2Direction = FORWARD;
              } else {
                m2Steps += -pos;
                m2Direction = BACKWARD;               
              }
            }
        }
        
        // Find the next command in input string
        command = strtok(0, "&");
    }

    while (m1Steps || m2Steps) {
      if (m1Steps > 0) {
        motor1.step(1, m1Direction, DOUBLE);
  
        m1Steps--;
      }
  
      if (m2Steps > 0) {
        motor2.step(1, m2Direction, DOUBLE);
  
        m2Steps--;
      }
    }
  }

  motor1.release();
  motor2.release();

  /*
    motor.step(100, FORWARD, SINGLE);  // Motor turn forward 100 single step
    motor.step(200, BACKWARD, SINGLE); // Motor turn backward 100 single step

    motor.step(100, FORWARD, DOUBLE);  // Motor turn forward 100 double step
    motor.step(100, BACKWARD, DOUBLE);  // Motor turn backward 100 double step

    motor.step(100, FORWARD, INTERLEAVE);  // Motor Turn forward interleave
    motor.step(100, BACKWARD, INTERLEAVE); // Motor Turn backward interleave

    motor.step(100, FORWARD, MICROSTEP);  // Motor Turn forward micro step
    motor.step(100, BACKWARD, MICROSTEP); // Motor Turn backward micro step
  */
}
