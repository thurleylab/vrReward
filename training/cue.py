import Image

white = (255, 255, 255)
black = (0, 0, 0)

# A4 size 297 x 210 mm
A4_x = 2480  # px
A4_y = 3508  # px

stripe_width = 30  # millimeters
width_in_px = int(stripe_width * (A4_y/297.))

img = Image.new('RGB', (A4_x, A4_y), white)

for stripe in range(A4_y/(2 * width_in_px) + (A4_y/width_in_px) % 2):
    for x in range(A4_x):
        for y in range(width_in_px):
            img.putpixel((x, stripe * 2*width_in_px + y), black)

img.save('stripes.png')
